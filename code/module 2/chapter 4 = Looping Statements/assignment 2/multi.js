function table1() {
    var a = document.getElementById("p");
    //deleting the rows
    while (a.rows.length > 1) {
        a.deleteRow(1);
    }
    //value of the number upto which u want to multiply
    var b = document.getElementById("b");
    var n = parseInt(b.value);
    //value of multiplier
    var c = document.getElementById("a");
    var m = parseInt(c.value);
    if (isNaN(m) || isNaN(n)) { //enetred value is not a number
        if (isNaN(n)) {
            alert("Entered value of the number upto which u want to multiply is not a number");
        }
        if (isNaN(m)) {
            alert("Entered value of the mutiplier is not a number");
        }
    }
    else { //enetred value is  a number
        for (var i = 1; i <= n; i++) {
            //multiplication of the multiplier upto which no u want
            var row = a.insertRow();
            var cell = row.insertCell();
            var t = document.createElement("input");
            t.type = "text";
            t.id = "t" + i;
            var fin = m * i;
            t.value = " " + m + "  *  " + i + "   =   " + fin;
            cell.appendChild(t);
        }
    }
}
//# sourceMappingURL=multi.js.map