var Timer;
(function (Timer) {
    //class to create a point
    class Point {
        constructor(x, y) {
            this.x = x;
            this.y = y;
        }
    }
    Timer.Point = Point;
    //class d to draw and also to update the dial
    class d {
        constructor(p1, p2, context) {
            this.p1 = p1;
            this.p2 = p2;
            this.context = context;
        }
        //function draw of class d to draw thw dial
        draw() {
            //circle
            this.context.beginPath();
            this.context.arc(this.p1.x, this.p1.y, 150, 0, Math.PI * 2);
            this.context.lineWidth = 5;
            this.context.fillStyle = "blue";
            this.context.fill();
            this.context.strokeStyle = "red";
            this.context.stroke();
            this.context.beginPath();
            this.context.arc(this.p1.x, this.p1.y, 100, 0, Math.PI * 2);
            this.context.lineWidth = 3;
            this.context.strokeStyle = "grey";
            this.context.stroke();
            //niddle 
            this.context.beginPath();
            this.context.moveTo(this.p1.x, this.p1.y);
            this.context.lineTo(this.p2.x, this.p2.y);
            this.context.lineWidth = 1;
            this.context.fillStyle = "black";
            this.context.fill();
            this.context.strokeStyle = "black";
            this.context.stroke();
            //degrees
            this.context.beginPath();
            this.context.font = "15pt Algerian";
            this.context.fillText("0", this.p1.x - 10, this.p1.y - 120);
            this.context.fillText("270", this.p1.x - 140, this.p1.y + 6);
            this.context.fillText("180", this.p1.x - 20, this.p1.y + 140);
            this.context.fillText("90", this.p1.x + 110, this.p1.y + 5);
            this.context.fillText("30", this.p1.x + 50, this.p1.y - 100);
            this.context.fillText("60", this.p1.x + 95, this.p1.y - 55);
            this.context.fillText("120", this.p1.x + 90, this.p1.y + 65);
            this.context.fillText("150", this.p1.x + 45, this.p1.y + 115);
            this.context.fillText("210", this.p1.x - 75, this.p1.y + 115);
            this.context.fillText("240", this.p1.x - 125, this.p1.y + 65);
            this.context.fillText("300", this.p1.x - 125, this.p1.y - 50);
            this.context.fillText("330", this.p1.x - 75, this.p1.y - 102);
            this.context.fillStyle = "white";
            this.context.fill();
        }
        //update the niddle of the dial
        update() {
            var i;
            var t1 = document.getElementById("t1");
            var c = t1.value;
            var len = Math.sqrt(Math.pow(this.p1.x - this.p2.x, 2) + Math.pow(this.p1.y - this.p2.y, 2));
            for (i = 0; i <= c; i++) {
                this.p2.x = this.p1.x + len * Math.cos((i - 90) * Math.PI / 180);
                this.p2.y = this.p1.y + len * Math.sin((i - 90) * Math.PI / 180);
            }
        }
    }
    Timer.d = d;
})(Timer || (Timer = {}));
//# sourceMappingURL=dial.js.map