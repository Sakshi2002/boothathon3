var canvas: HTMLCanvasElement = <HTMLCanvasElement>document.getElementById("mycanvas");
var context: CanvasRenderingContext2D = canvas.getContext("2d");
var p1=new Timer.Point(500,250);
var p2=new Timer.Point(500,150);
var d=new Timer.d(p1,p2,context);
d.draw();
var t1:HTMLInputElement=<HTMLInputElement>document.getElementById("t1");
function r(){
    var a=parseFloat(t1.value);
    if(isNaN(a) || a>360 || a<0 ){
        if(isNaN(a)){
            alert("Entered value is not number plz enter the number")
        }
        if(a>360 || a<0){
            alert("Entered value is not in the range 0 to 360")
        }
    }
    else{
        animate();
    }
}
function animate(){
    context.clearRect(0,0,canvas.width,canvas.height);
    d.draw();
    d.update();
    window.requestAnimationFrame(animate);
}