var force_arr = []; //array for force
var angle_arr = []; //array for angle
//draw table...
function fromation_of_table() {
    var a = document.getElementById("s");
    //clearing table
    while (a.rows.length > 1) {
        a.deleteRow(1);
    }
    let n1 = document.getElementById("row");
    var n = parseInt(n1.value);
    var i;
    //if value entered of number of force to be inputed is not anumber then
    if (isNaN(n)) {
        alert("Entered value is not a number plz enter a number");
    }
    // if it is a number then
    else {
        for (i = 1; i <= n; i++) {
            //adding rows
            var row = a.insertRow();
            //inserting colunm 1 for force
            var cell = row.insertCell();
            var t = document.createElement("input");
            t.type = "text";
            t.id = "t" + i;
            t.style.backgroundColor = "red";
            cell.appendChild(t);
            //inserting colunm 2 for its angle
            var cell = row.insertCell();
            var t = document.createElement("input");
            t.type = "text";
            t.id = "t" + i + i;
            t.style.backgroundColor = "red";
            cell.appendChild(t);
        }
    }
}
function resultant_force() {
    let n1 = document.getElementById("row");
    var n = parseInt(n1.value);
    var Fx = 0;
    var Fy = 0;
    var i;
    //defining array of forces and angle of each
    for (i = 1; i <= n; i++) {
        var d1 = document.getElementById("t" + i);
        var z1 = document.getElementById("t" + i + i);
        var d = parseInt(d1.value);
        var z = parseInt(z1.value);
        force_arr.push(d);
        angle_arr.push(z);
    }
    console.log(force_arr, angle_arr);
    //accessing each element of array and calculating summation of x and y forces
    for (i = 0; i < n; i++) {
        //if entered value of forces and angle is not a number then
        if (isNaN(force_arr[i]) || isNaN(angle_arr[i])) {
            if (isNaN(force_arr[i])) {
                var o = i + 1;
                alert("Entered value of force F" + o + " is not a number");
            }
            if (isNaN(angle_arr[i])) {
                var o = i + 1;
                alert("Entered value of angle A" + o + " is not a number");
            }
        }
        //if entered value of force and angle is a number
        else {
            Fx = Fx + force_arr[i] * Math.cos(Math.PI / 180 * angle_arr[i]);
            Fy = Fy + force_arr[i] * Math.sin(Math.PI / 180 * angle_arr[i]);
            //resultwnt force and angle
            var R = Math.sqrt(Math.pow(Fx, 2) + Math.pow(Fy, 2));
            var b = Math.atan2(Fy, Fx);
            //display output
            document.getElementById("p").innerHTML = "Resultant of the froces is :<u style=color:darkviolet;> " + R + "</u><br/>" + "Angle of the resultant force is : <u style=color:darkviolet;>" + b * 180 / Math.PI + "</u>";
        }
    }
}
//# sourceMappingURL=force.js.map