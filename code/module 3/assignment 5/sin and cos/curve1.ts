namespace Cruve{
    //class to create a point
    export class Point {

        public x:number;
        public y:number;
        constructor(x:number,y:number){
            this.x=x;
            this.y=y;
        }
    }
    //class sin to create sin curve
    export class sin{
        public p1:Point;
        public context:CanvasRenderingContext2D;
        private i:number;
        constructor(p1,context){
            this.p1=p1;
            this.context=context;
            this.i;
        }
        //function of class sin to draw sin cruve
        draw(){
            //rectangle
            this.context.beginPath();
            this.context.rect(this.p1.x-60, this.p1.y-200,550,400);
            this.context.fillStyle="yellow";
            this.context.fill();
            this.context.lineWidth=4;
            this.context.strokeStyle="green"
            this.context.stroke();

            //write sine curve
            this.context.font = "60pt Brush Script MT";
            this.context.fillStyle="black"
            this.context.fillText("sine wave", this.p1.x, this.p1.y-250);
            this.context.font = "30pt Brush Script MT";
            this.context.fillText("x", this.p1.x+415, this.p1.y);
            this.context.fillText("y", this.p1.x, this.p1.y-165);

            //values on y-axis
            this.context.font = "20pt Constantia";
            this.context.fillText("1", this.p1.x-15, this.p1.y-100);
            this.context.fillText("0", this.p1.x-17, this.p1.y);
            this.context.fillText("-1", this.p1.x-23, this.p1.y+100);
            //values on x-axies
            this.context.fillText("90", this.p1.x+85, this.p1.y+20);
            this.context.fillText("180", this.p1.x+175, this.p1.y+20);
            this.context.fillText("270", this.p1.x+265, this.p1.y+20);
            this.context.fillText("360", this.p1.x+355, this.p1.y+20);
            //draw x-axis
            this.context.beginPath();
            this.context.moveTo(this.p1.x, this.p1.y);
            this.context.lineTo(this.p1.x+400, this.p1.y);
            this.context.strokeStyle = "black";
            this.context.lineWidth = 5;
            this.context.stroke();
            //draw y-axis
            this.context.beginPath();
            this.context.moveTo(this.p1.x,this.p1.y-150);
            this.context.lineTo(this.p1.x, this.p1.y+150);
            this.context.strokeStyle = "black";
            this.context.lineWidth = 5;
            this.context.stroke();
            

            this.context.save();
            this.context.translate(this.p1.x, this.p1.y);
            this.context.scale(1, -1);
            //sin curve
            this.context.beginPath();
            for (this.i=0; this.i <= 360; this.i++) {
                this.context.lineTo(this.i, 100 * Math.sin(this.i * Math.PI / 180));
            }
            this.context.lineWidth = 3
            this.context.strokeStyle = "red";
            this.context.stroke();
            this.context.restore();
        }
    }
    //class cos to create cos curve
    export class cos{
        public p1:Point;
        public context:CanvasRenderingContext2D;
        private i:number;
        constructor(p1,context){
            this.p1=p1;
            this.context=context;
            this.i;
        }
        //function of class cos to draw cos cruve
        draw(){
            //rectangle
            this.context.beginPath();
            this.context.rect(this.p1.x-60, this.p1.y-200,550,400);
            this.context.fillStyle="yellow";
            this.context.fill();
            this.context.lineWidth=4;
            this.context.strokeStyle="green"
            this.context.stroke();

            //write cos curve
            this.context.fillStyle="black"
            this.context.font = "60pt Brush Script MT";
            this.context.fillText("Cos wave", this.p1.x, this.p1.y-250);
            this.context.font = "30pt Brush Script MT";
            this.context.fillText("x", this.p1.x+415, this.p1.y);
            this.context.fillText("y", this.p1.x, this.p1.y-165);
            //values on y-axis
            this.context.font = "20pt Constantia";
            this.context.fillText("1", this.p1.x-15, this.p1.y-100);
            this.context.fillText("0", this.p1.x-17, this.p1.y);
            this.context.fillText("-1", this.p1.x-23, this.p1.y+100);
            //values on x-axies
            this.context.fillText("90", this.p1.x+85, this.p1.y+20);
            this.context.fillText("180", this.p1.x+175, this.p1.y+20);
            this.context.fillText("270", this.p1.x+265, this.p1.y+20);
            this.context.fillText("360", this.p1.x+355, this.p1.y+20);
            //draw x-axis
            this.context.beginPath();
            this.context.moveTo(this.p1.x, this.p1.y);
            this.context.lineTo(this.p1.x+400, this.p1.y);
            this.context.strokeStyle = "black";
            this.context.lineWidth = 5;
            this.context.stroke();
            //draw y-axis
            this.context.beginPath();
            this.context.moveTo(this.p1.x,this.p1.y-150);
            this.context.lineTo(this.p1.x, this.p1.y+150);
            this.context.strokeStyle = "black";
            this.context.lineWidth = 5;
            this.context.stroke();
            
            this.context.save();
            this.context.translate(this.p1.x, this.p1.y);
            this.context.scale(1, -1);
            //draw sin curve
            this.context.beginPath();
            for (this.i=0; this.i <= 360; this.i++) {
                this.context.lineTo(this.i, 100 * Math.cos(this.i * Math.PI / 180));
            }
            this.context.lineWidth = 3
            this.context.strokeStyle = "red";
            this.context.stroke();
            this.context.restore();
        }
    }
}