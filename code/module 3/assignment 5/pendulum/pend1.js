var Pend;
(function (Pend) {
    //class to create points
    class Point {
        constructor(x, y) {
            this.x = x;
            this.y = y;
        }
    }
    Pend.Point = Point;
    //class circle to create pendulum
    class circle {
        constructor(p1, p2, context) {
            this.p1 = p1;
            this.p2 = p2;
            this.context = context;
            this.i = 90;
            this.r = 50,
                this.left = true;
            this.right = false;
        }
        //function draw of class circle to draw pendulum
        draw() {
            //vertical line of pendulum
            this.context.beginPath();
            this.context.moveTo(this.p1.x, this.p1.y);
            this.context.lineTo(this.p2.x, this.p2.y);
            this.context.lineWidth = 3;
            this.context.strokeStyle = "darkviolet";
            this.context.stroke();
            //horizontal line
            this.context.beginPath();
            this.context.moveTo(this.p1.x - 150, this.p1.y);
            this.context.lineTo(this.p1.x + 150, this.p1.y);
            this.context.lineWidth = 2;
            this.context.strokeStyle = "black";
            this.context.stroke();
            //circle
            this.context.beginPath();
            this.context.arc(this.p2.x, this.p2.y, this.r, 0, Math.PI * 2);
            this.context.lineWidth = 4;
            this.context.fillStyle = "blue";
            this.context.fill();
            this.context.strokeStyle = "darkblue";
            this.context.stroke();
        }
        //function update of class circle to rotate the pendulum
        update() {
            var len = Math.sqrt(Math.pow(this.p1.x - this.p2.x, 2) + Math.pow(this.p1.y - this.p2.y, 2));
            if (this.i < 120 && this.left) {
                this.i++;
                this.p2.x = this.p1.x + len * Math.cos(this.i * Math.PI / 180);
                this.p2.y = this.p1.y + len * Math.sin(+this.i * Math.PI / 180);
            }
            if (this.i == 120) {
                this.left = false;
                this.right = true;
            }
            if (this.i >= 60 && this.right) {
                this.i--;
                this.p2.x = this.p1.x + len * Math.cos(this.i * Math.PI / 180);
                this.p2.y = this.p1.y + len * Math.sin(this.i * Math.PI / 180);
            }
            if (this.i == 60) {
                this.left = true;
                this.right = false;
            }
        }
    }
    Pend.circle = circle;
})(Pend || (Pend = {}));
//# sourceMappingURL=pend1.js.map