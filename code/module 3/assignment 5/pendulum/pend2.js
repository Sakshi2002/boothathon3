var canvas = document.getElementById("mycanvas");
var context = canvas.getContext("2d");
var p1;
var p2;
var c;
p1 = new Pend.Point(500, 100);
p2 = new Pend.Point(500, 400);
c = new Pend.circle(p1, p2, context);
//function draw called to draw pendulum
c.draw();
function rotate() {
    //function animate is called
    animate();
}
function animate() {
    //clearing the canvas
    context.clearRect(0, 0, canvas.width, canvas.height);
    console.log(1);
    //function draw and update called to rotate the pendulum
    c.draw();
    c.update();
    window.requestAnimationFrame(animate);
}
//# sourceMappingURL=pend2.js.map