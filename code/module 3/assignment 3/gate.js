var Gate;
(function (Gate) {
    //class And to draw AND Gate
    class And {
        constructor(x, y, context) {
            this.x = x;
            this.context = context;
            this.y = y;
        }
        //in class And function draw to Create the AND gate
        draw() {
            this.context.beginPath();
            this.context.arc(this.x, this.y, 30, 1.5 * Math.PI, 0.5 * Math.PI);
            this.context.lineWidth = 4;
            this.context.strokeStyle = "black";
            this.context.stroke();
            this.context.beginPath();
            this.context.moveTo(this.x, this.y + 30);
            this.context.lineTo(this.x - 60, this.y + 30);
            this.context.lineWidth = 4;
            this.context.strokeStyle = "black";
            this.context.stroke();
            this.context.beginPath();
            this.context.moveTo(this.x, this.y - 30);
            this.context.lineTo(this.x - 60, this.y - 30);
            this.context.lineWidth = 4;
            this.context.strokeStyle = "black";
            this.context.stroke();
            this.context.beginPath();
            this.context.moveTo(this.x - 60, this.y + 30);
            this.context.lineTo(this.x - 60, this.y - 30);
            this.context.lineWidth = 4;
            this.context.strokeStyle = "black";
            this.context.stroke();
            this.context.beginPath();
            this.context.moveTo(this.x - 60, this.y + 15);
            this.context.lineTo(this.x - 90, this.y + 15);
            this.context.lineWidth = 4;
            this.context.strokeStyle = "black";
            this.context.stroke();
            this.context.beginPath();
            this.context.moveTo(this.x - 60, this.y - 15);
            this.context.lineTo(this.x - 90, this.y - 15);
            this.context.lineWidth = 4;
            this.context.strokeStyle = "black";
            this.context.stroke();
            this.context.beginPath();
            this.context.moveTo(this.x + 30, this.y);
            this.context.lineTo(this.x + 60, this.y);
            this.context.lineWidth = 4;
            this.context.strokeStyle = "black";
            this.context.stroke();
        }
    }
    Gate.And = And;
    //class Not to draw 
    class Not {
        constructor(x, y, context) {
            this.x = x;
            this.context = context;
            this.y = y;
        }
        //in class Not function draw to Create the NOT gate
        draw() {
            this.context.beginPath();
            this.context.moveTo(this.x, this.y);
            this.context.lineTo(this.x - 60, this.y + 30);
            this.context.lineWidth = 4;
            this.context.strokeStyle = "black";
            this.context.stroke();
            this.context.beginPath();
            this.context.moveTo(this.x, this.y);
            this.context.lineTo(this.x - 60, this.y - 30);
            this.context.lineWidth = 4;
            this.context.strokeStyle = "black";
            this.context.stroke();
            this.context.beginPath();
            this.context.moveTo(this.x - 60, this.y + 30);
            this.context.lineTo(this.x - 60, this.y - 30);
            this.context.lineWidth = 4;
            this.context.strokeStyle = "black";
            this.context.stroke();
            this.context.beginPath();
            this.context.moveTo(this.x, this.y);
            this.context.lineTo(this.x + 30, this.y);
            this.context.lineWidth = 4;
            this.context.strokeStyle = "black";
            this.context.stroke();
            this.context.beginPath();
            this.context.moveTo(this.x - 60, this.y);
            this.context.lineTo(this.x - 90, this.y);
            this.context.lineWidth = 2;
            this.context.strokeStyle = "black";
            this.context.stroke();
        }
    }
    Gate.Not = Not;
})(Gate || (Gate = {}));
//# sourceMappingURL=gate.js.map