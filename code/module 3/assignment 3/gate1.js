var canvas = document.getElementById("mycanvas");
var context = canvas.getContext("2d");
context.save();
context.translate(0, 500);
context.scale(1, -1);
//function for creating AND gate
function and() {
    //clearing the data of the canvas
    context.clearRect(0, 0, canvas.width, canvas.height);
    var x = +prompt("Enter the X coordinate between 100 to 1145");
    var y = +prompt("Enter the Y coordinate between 50 to 450");
    //checking whether entered value of X and Y coordinate is number or not and is between thw given range
    //if it is not then
    if ((x > 1145) || (x < 100) || isNaN(x) || (y > 450) || (y < 50) || isNaN(y)) {
        if (isNaN(x)) {
            alert("Entered value of X-cordinate is not number plz eneterd the number ");
        }
        if ((x > 1145) || (x < 100)) {
            alert("enter a valid choice X-cordinate should be between 100 to 1145");
        }
        if (isNaN(y)) {
            alert("Entered value of Y-cordinate is not number plz eneterd the number ");
        }
        if ((y > 450) || (y < 50)) {
            alert("enter a valid choice Y-cordinate should be between 50 to 450");
        }
    }
    //if it is then
    else {
        var a = new Gate.And(x, y, context);
        a.draw(); //function is called to draw AND gate
    }
}
//function for creating NOT gate
function not() {
    //clearing the data of the canvas
    context.clearRect(0, 0, canvas.width, canvas.height);
    var x = +prompt("Enter the X coordinate between 100 to 1145");
    var y = +prompt("Enter the Y coordinate between 50 to 450");
    //checking whether entered value of X and Y coordinate is number or not and is between thw given range
    //if it is not then
    if ((x > 1145) || (x < 100) || isNaN(x) || (y > 450) || (y < 50) || isNaN(y)) {
        if (isNaN(x)) {
            alert("Entered value of X-cordinate is not number plz eneterd the number ");
        }
        if ((x > 1145) || (x < 100)) {
            alert("enter a valid choice X-cordinate should be between 100 to 1145");
        }
        if (isNaN(y)) {
            alert("Entered value of Y-cordinate is not number plz eneterd the number ");
        }
        if ((y > 450) || (y < 50)) {
            alert("enter a valid choice Y-cordinate should be between 50 to 450");
        }
    }
    //if it is then
    else {
        var b = new Gate.Not(x, y, context);
        b.draw(); //function is called to draw NOT gate
    }
}
//# sourceMappingURL=gate1.js.map