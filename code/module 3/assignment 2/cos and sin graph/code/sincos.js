//sine graph
var sin = [];
for (let i = 0; i <= 360; i += 15) {
    sin.push({
        x: i,
        y: Math.sin(i * Math.PI / 180),
    });
}
drawgraph("l1", sin, "x value", "y value");
//cos graph
var cos = [];
for (let i = 0; i <= 360; i += 15) {
    cos.push({
        x: i,
        y: Math.cos(i * Math.PI / 180),
    });
}
drawgraph("l2", cos, "x value", "y value");
//# sourceMappingURL=sincos.js.map