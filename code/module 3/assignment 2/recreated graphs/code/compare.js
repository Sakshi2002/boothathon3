//comparision of x^2+x
var datapoints1 = [];
var datapoints2 = [];
//pushing data in array datapoints1 and datapoints2
for (let i = 1; i <= 10; i++) {
    datapoints1.push({ x: i, y: i * i });
    datapoints2.push({ x: i, y: i * i + i });
}
//drawing graph using array datapoints1
drawgraph("l1", datapoints1, "x ", " x square");
//drawing graph using array datapoints1 and datapoints2 for comparision of x^2+x     
function draw1() {
    drawgraph2("l2", datapoints1, datapoints2, "X-axis", "Y-axis", "comparision", "x^2", "x^2+x");
}
//log graph
var data2 = [];
//pushing data in array data2
data2.push({ x: 10, y: 10 });
data2.push({ x: 15, y: 640 });
data2.push({ x: 22, y: 10240 });
data2.push({ x: 31, y: 80240 });
data2.push({ x: 42, y: 162400 });
data2.push({ x: 55, y: 200000 });
data2.push({ x: 70, y: 220000 });
data2.push({ x: 87, y: 230000 });
data2.push({ x: 100, y: 256000 });
graphlogy("log", data2, "X-axis", "Y-axis");
//square graph
var datapoints3 = [];
//pushing data in array datapoints3
datapoints3.push({ x: 6, y: 6 });
datapoints3.push({ x: 2, y: 6 });
datapoints3.push({ x: 2, y: 2 });
datapoints3.push({ x: 6, y: 2 });
datapoints3.push({ x: 6, y: 6 });
graphline("l3", datapoints3, "X-AXIS ", "Y-AXIS");
//multiple log graph
var data1 = [];
//pushing data in array data1
data1.push({ x: 10, y: 10 });
data1.push({ x: 15, y: 640 });
data1.push({ x: 22, y: 10240 });
data1.push({ x: 31, y: 80240 });
data1.push({ x: 42, y: 162400 });
data1.push({ x: 55, y: 200000 });
data1.push({ x: 70, y: 220000 });
data1.push({ x: 87, y: 230000 });
data1.push({ x: 100, y: 256000 });
var data3 = [];
//pushing data in array data3
data3.push({ x: 10, y: 10 });
data3.push({ x: 15, y: 840 });
data3.push({ x: 22, y: 15240 });
data3.push({ x: 31, y: 99240 });
data3.push({ x: 42, y: 202400 });
data3.push({ x: 55, y: 300000 });
data3.push({ x: 70, y: 330000 });
data3.push({ x: 87, y: 400000 });
data3.push({ x: 100, y: 456000 });
var data = [];
//pushing data of array data3 and data1 in array data
data.push({ type: "line", dataPoints: data1 });
data.push({ type: "line", dataPoints: data3 });
logymultiple("Loggraph", "X-axis", "Y-axis", data, "log1");
//# sourceMappingURL=compare.js.map