declare var graphline; //framework function

var datapoints1:{x:number;y:number}[] = [];
var datapoints2:{x:number;y:number}[] = [];

datapoints1.push({ x: 10, y: 10 });
datapoints1.push({ x: 20, y: 10 });
datapoints1.push({ x: 20, y: 20 });
datapoints1.push({ x: 10, y: 20 });
datapoints1.push({ x: 10, y: 10 });

var i;
for(i=10;i<=60;i++){
    datapoints1.push({ x: i, y: i });
}
var data=[];
data.push({
    type: "line",
    color:"blue",
    xValueType: "Float",
    showInLegend: false,
    name: "f(x)",
    markerSize: 1,
    dataPoints: datapoints1
})
                        
data.push({
    type: "line",
    color:"red",
    xValueType: "Float",
    showInLegend: false,
    name: "f(x1)",
    markerSize: 1,
    dataPoints: datapoints2
})                 
graphline("l1", data, "x axis", "y-axis");